#!/bin/bash

rm arcolinux_repo_xlarge*

echo "repo-add"
repo-add -s -n -R arcolinux_repo_xlarge.db.tar.gz *.pkg.tar.zst

sleep 1

rm arcolinux_repo_xlarge.db
rm arcolinux_repo_xlarge.db.sig

rm arcolinux_repo_xlarge.files
rm arcolinux_repo_xlarge.files.sig

mv arcolinux_repo_xlarge.db.tar.gz arcolinux_repo_xlarge.db
mv arcolinux_repo_xlarge.db.tar.gz.sig arcolinux_repo_xlarge.db.sig

mv arcolinux_repo_xlarge.files.tar.gz arcolinux_repo_xlarge.files
mv arcolinux_repo_xlarge.files.tar.gz.sig arcolinux_repo_xlarge.files.sig

echo "####################################"
echo "Repo Updated!!"
echo "####################################"
